package com.company;

public class ContaBancaria extends Cliente{

    private int id;
    private double saldo;
    private double valorSaque;
    private double valorDeposito;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getValorSaque() {
        return valorSaque;
    }

    public void setValorSaque(double valorSaque) {
        this.valorSaque = valorSaque;
    }

    public double getValorDeposito() {
        return valorDeposito;
    }

    public void setValorDeposito(double valorDeposito) {
        this.valorDeposito = valorDeposito;
    }

    public ContaBancaria(int id, String nome, int idade, double saldo, double valorSaque, double valorDeposito) {
        super(id, nome, idade);
        this.saldo = saldo;
        this.valorSaque = valorSaque;
        this.valorDeposito = valorDeposito;
    }

    public double sacar(double valorSaque){
        return saldo - valorSaque;
    }

    public double depositar(double valorDepositar){
        return saldo + valorDepositar;
    }


}
