package com.company;

public class Cliente {

    private int id;
    private String nome;
    private int idade;

    public Cliente(int id, String nome, int idade) {
        this.id = id;
        this.nome = nome;
        this.idade = idade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {

        return this.idade;

    }

    public void setIdade(int idade) {

        if(idade >= 18){
            this.idade = idade;
        }else{
            this.idade = 0;
        }

    }
}
