package com.company;

import javax.swing.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        //Cliente cliente = new Cliente(1, "Rodrigo", 30);

        Scanner scanner = new Scanner(System.in);

        System.out.println("********ITAU UNIBANCO************");
        System.out.println("Informe a idade do cliente: ");
        System.out.println("*********************************");
        int idade = scanner.nextInt();

        ContaBancaria cliente = new ContaBancaria(3030, "Rodrigo Sampaio", idade,0, 0, 1000);

        cliente.setIdade(idade);

        if (cliente.getIdade() == 0) {
            System.out.println("********ITAU UNIBANCO************");
            System.out.println("Não é possivel criar o cliente");
            System.out.println("*********************************");
            return;

        } else {

            System.out.println("********ITAU UNIBANCO************");
            System.out.println("Informe: 1 = Saque / 2 = Deposit0");
            System.out.println("*********************************");
            int acao = scanner.nextInt();

            double saldoFinal;

            if (acao == 1) {

                System.out.println("Inform o valor do saque: ");
                String saque = scanner.nextLine();
                saldoFinal = Double.parseDouble(saque);
                cliente.sacar(saldoFinal);

            } else {

                System.out.println("Inform o valor do saque: ");
                String saque = scanner.nextLine();
                saldoFinal = Double.parseDouble(saque);
                cliente.depositar(saldoFinal);

            }

        }
    }
}
